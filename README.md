# Моделирование пассажиропотока на Восточном вокзале


В рамках учебной практики ознакомилась с имитационным моделированием. Для создания модели использовалась AnyLogic 8 PLE.

В работе применялась теория вероятности, в частности треугольное распределение.

С работой модели можно ознакомиться в файле "simulation modeling railway".

Состав участников проекта: Махоткина Е.Д., Терёшкина Н.А., Шабарина М.А.
